# while trying to burn time at work

$workToBeDone = 100     # Percent, should be 0% by end of day (but will probably be done much sooner)
                #   If you complete it early, you will be given more work.
$goodEmployeeRating = 90    # Percent, affects chance of being reprimanded
$alienation         # From the rest of your life, affects depression
$stealth = 100          # Chance of being caught; affects goodEmployeeRating
$energy = 100           # Can't work if energy is too low
$depression = 50        # Percent, can't work if too depressed

$timeHours = 8          # Core hours are 9 to 4
$timeMinutes = 0
$timeRemainingHours = 8     # Must "work" (be in office) for 8 hours per day
$timeRemainingMinutes = 0

label cycle_start:
    # Make a dialog box that shows the stats... 
    # Time $timeHours : $timeMinutes
    # Work time remaining $timeRemainingHours : $timeRemainingMinutes
    # Energy $energy
    # Amount of work left $workToBeDone 

    menu:
        "Do some work":
            # goodEmployeeRating
            # energy
            menu:
                "Little task":
                "Big task":

        "Take a break":
            # goodEmployeeRating
            # energy
            menu:
                "Coffee break":
                "Lunch break":

        "Slack off at your desk":
            # goodEmployeeRating
            # energy
            # stealth
            menu:
                "Read articles":
                "Watch videos":
                "Sketch":

        "Go home":
            # Have you met the work hours?
            # Affect "goodEmployeeRating" if you leave too early.


    jump cycle_start

    # Choose when to arrive at work next day
    # Energy is restored at end of day based on # of hours at home in the evening you get.
    # Random event chance: Emergency errand, social event (fun or not fun), chores, or lazy day.
