﻿label gamedata_setup:

    # Variables
    $ corporateWords1 = [ "Synergy", "Digital", "Central", "Info", "Disruptive", "Dynamic", "Agile", "Global", "Data", "Net", "Cloud" ]
    $ corporateWords2 = [ "Solutions", "Technologies", "Systems", "Software", "Media", "Group", "Engineering", "Labs" ]
    $ corporateWords3 = [ "LLC", "Inc", "Co", "Corp" ]
    
    $ companyName = renpy.random.choice( corporateWords1 ) + " " + renpy.random.choice( corporateWords2 ) + ", " + renpy.random.choice( corporateWords3 )
    $ yourName = ""
    $ pronounSet = ""

    # while trying to burn time at work

    $ workToBeDone = 0              # Tasks, should be 0 by end of day (but will probably be done much sooner)
                                    #   If you complete it early, you will be given more work.
    $ goodEmployeeRating = 90       # Percent, affects chance of being reprimanded
    $ alienation = 0                # From the rest of your life, affects depression
    $ stealth = 100                 # Chance of being caught; affects goodEmployeeRating
    $ energy = 100                  # Can't work if energy is too low
    $ depression = 50               # Percent, can't work if too depressed
    $ lunchesTakenToday = 0
    $ totalBreaksToday = 0
    $ totalNoticableBreaktimeToday = 0
    $ totalDiscreetBreaktimeToday = 0
    $ day = 1
    $ days = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ]
    $ gameOver = False

    $ timeHours = 0               # Core hours are 9 (9:00) to 4 (16:00)
    $ timeMinutes = 0
    $ timeHoursAmPm = 0
    $ timeAmPm = "am"
    $ timeText = ""
    $ timeRemainingMinutes = 8 * 60
    $ timeRemainingText = ""
    $ pto = 3
    
    # Character names
    define you = Character("You")
    define boss = Character("Manager")
    define coworker = Character("Coworker")
    define hr = Character("Human Resources")
    define secretary = Character("Front desk")
    
    # Images
    image bg computer_chatting  = "images/backgrounds/computer_chatting.jpg"
    image bg computer_coding    = "images/backgrounds/computer_coding.jpg"
    image bg computer_videos    = "images/backgrounds/computer_videos.jpg"
    image bg computer_web       = "images/backgrounds/computer_web.jpg"
    image bg bedroom_bluesky    = "images/backgrounds/room_bluesky.jpg"
    image bg bedroom_greysky    = "images/backgrounds/room_greysky.jpg"
    image bg breakroom          = "images/backgrounds/breakroom.jpg"
    image bg outside            = "images/backgrounds/outside.jpg"
    image bg bathroom           = "images/backgrounds/bathroom.jpg"
    image bg lunch              = "images/backgrounds/lunch.jpg"
    image bg black              = "images/backgrounds/black.jpg"
    
    return
    
# Workaround since I couldn't figure out how to create a variable in my
# stat window definition AND draw that text.
label format_time:
    
    while timeMinutes >= 60:
        $ timeHours += 1
        $ timeMinutes -= 60

    if timeHours > 12:
        $ timeHoursAmPm = timeHours - 12
        $ timeAmPm = "pm"
    elif timeHours == 12:
        $ timeHoursAmPm = timeHours
        $ timeAmPm = "pm"
    else:
        $ timeHoursAmPm = timeHours
        $ timeAmPm = "am"
    
    if timeMinutes < 10:
        $ timeText = str( timeHoursAmPm ) + ":0" + str( timeMinutes ) + " " + timeAmPm
    else:
        $ timeText = str( timeHoursAmPm ) + ":" + str( timeMinutes ) + " " + timeAmPm

    return

label format_timeLeft:
    # $ timeRemainingMinutes = 8 * 60
    $ hours = timeRemainingMinutes / 60
    $ minutes = timeRemainingMinutes % 60
    
    if minutes < 10:
        $ timeRemainingText = str( hours ) + ":0" + str( minutes )
    else:
        $ timeRemainingText = str( hours ) + ":" + str( minutes )

    return
    
label restrict_vars:
    if energy > 100:
        $ energy = 100
    elif energy < 0:
        $ energy = 0
    
    if depression > 100:
        $ depression = 100
    elif depression < 0:
        $ depression = 0
        
    if stealth > 100:
        $ stealth = 100
    elif stealth < 0:
        $ stealth = 0
        
    if alienation > 100:
        $ alienation = 100
    elif alienation < 0:
        $ alienation = 0
        
    if timeRemainingMinutes < 0:
        $ timeRemainingMinutes = 0
    
