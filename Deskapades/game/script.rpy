﻿# ----------------------------------------------------------- GAME START
label start:
    call gamedata_setup from _call_gamedata_setup
    jump day_start

# --------------------------------------------------------- START OF DAY
label day_start:
    call reset_daily_variables from _call_reset_daily_variables
    
    if gameOver:
        return
    
    if day < 6: # Weekday
        call alarm_clock from _call_alarm_clock
    call set_weather from _call_set_weather
    call check_depression from _call_check_depression
    call plan_arrival from _call_plan_arrival

    jump cycle_start


# --------------------------------------------------------- START OF DAY
# --------------------------------------------------------- Alarm clock
label alarm_clock:
    $ wake_up = False
    $ first_wake = True
    
    while timeHours <= 8 and wake_up == False:
        
        play sound "sounds/alarm.ogg"
        show bg black with fade
        
        if first_wake:
            $ first_wake = False
            if day == 1:    
                "It's Monday. Crap."
            elif day == 2:
                "It's Tuesday. Eh."
            elif day == 3:
                "It's Wednesday. Half way there."
            elif day == 4:
                "It's Thursday. Almost there."
            elif day == 5:
                "It's Friday. Finally."
        
        call morning_time from _morning_time
        $ renpy.pause( 1.0 )
        
        if timeHours == 8:
            you "OK, I can't sleep anymore. I have to get ready for work."
            return
        
        menu:
            "Get up.":
                if depression > 75 and timeHours < 8:
                    you "I can't."
                    $ timeHours += 1
                
                elif depression > 50 and timeHours < 7:
                    you "I don't feel like it."
                    $ timeHours += 1
                    
                else:
                    you "Might as well get up..."
                    $ wake_up = True
            
            "Sleep more.":
                you "I need more sleep."
                $ timeHours += 1
    
    if timeHours < 7:
        $ energyChange = 5
        $ energy += energyChange
        $ depressionChange = 5
        $ depression += depressionChange
        
        play sound "sounds/happy.ogg"
        you "You wake up with enough time to make breakfast and a cup of coffee and just {i}exist{/i} for a little bit before work. \n{color=#00FF00} Energy +[energyChange] {/color}       {color=#00FF00} Depression -[depressionChange] {/color}"
            
    return

# --------------------------------------------------------- START OF DAY
# --------------------------------------------------------- Set weather
label set_weather:
    $ weather = renpy.random.randint( 1, 3 )
    
    if day < 6:
        play music "music/Dissappointment.mp3" fadein 1.0
    
    if weather == 1:    # Sunny
        scene bg bedroom_bluesky with dissolve

    elif weather == 2:  # Rainy
        scene bg bedroom_bluesky with dissolve

    else:               # Gray sky
        scene bg bedroom_greysky with dissolve


    if day == 6:
        
        $ energyChange = renpy.random.randint( 15, 25 )
        $ energy += energyChange    
        $ depressionChange = renpy.random.randint( 15, 25 )
        $ depression -= depressionChange
        
        "It's Saturday.\n{color=#00FF00} Depression -[depressionChange] {/color}      {color=#00FF00} Energy +[energyChange] {/color}"
             
        $ day += 1
        if day > 7:
            $ day = 1
        
        scene bg black with dissolve
        jump day_start
        
    elif day == 7:
        
        $ energyChange = renpy.random.randint( 15, 25 )
        $ energy += energyChange    
        $ depressionChange = renpy.random.randint( 15, 25 )
        $ depression -= depressionChange
        
        "It's Sunday.\n{color=#00FF00} Depression -[depressionChange] {/color}      {color=#00FF00} Energy +[energyChange] {/color}"

        $ day += 1
        if day > 7:
            $ day = 1
        
        scene bg black with fade
        jump day_start
        
    if weather == 1:    # Sunny
        $ energyChange = 5
        $ energy += energyChange
        
        play sound "sounds/happy.ogg"
        you "It's a nice day today! \n{color=#00FF00} Energy +[energyChange] {/color}"

    elif weather == 2:  # Rainy
        $ energyChange = 5
        $ energy += energyChange
        
        play sound "sounds/happy.ogg"
        you "The light rain makes a relaxing sound! \n{color=#00FF00} Energy +[energyChange] {/color}"

    else:               # Gray sky
        $ energyChange = 5
        $ energy += energyChange
        
        play sound "sounds/sad.ogg"
        you "Ugh, gray skies. It's such a gloomy day today. \n{color=#FF0000} Energy -[energyChange] {/color}"


    return


# --------------------------------------------------------- START OF DAY
# --------------------------------------------------------- Check depression
label check_depression:
    if depression >= 90:
        play sound "sounds/sad.ogg"
        you "I'm too depressed to go to work today... I'm going to call in sick... \n{color=#00FF00} Depression -[depressionChange] {/color}      {color=#00FF00} Energy +[energyChange] {/color}"
        $ workToBeDone = workToBeDone + renpy.random.randint( 4, 12 )
        
        $ energyChange = 10
        $ energy += energyChange
        $ depressionChange = 5
        $ depression -= depressionChange
        
        $ pto -= 1
        

        if pto < 0:
            $ goodEmployeeRating -= 10

        jump end_of_day

    return


# --------------------------------------------------------- START OF DAY
# --------------------------------------------------------- Plan arrival
label plan_arrival:
    you "What time should I arrive at the office today?"

    menu:
        "Arrive at 7:00" if timeHours < 7:
            $ timeHours = 7
            
            $ energyChange = 5
            $ energy += energyChange
            
            play sound "sounds/happy.ogg"
            "You know by getting to the office earlier, you can leave earlier. Something to look forward to.      {color=#00FF00} Energy +[energyChange]{/color}"

        "Arrive at 8:00" if timeHours < 8:
            $ timeHours = 8

        "Arrive at 9:00":
            $ timeHours = 9
            
            $ energyChange = 2
            $ energy -= energyChange
            $ depressionChange = 5
            $ depression += depressionChange
            
            play sound "sounds/sad.ogg"
            "You're too tired to go in any earlier, but unfortunately you know this means you'll spend more of your daylight at the office. \n{color=#FF0000} Energy -[energyChange]{/color}    {color=#FF0000} Depression +[depressionChange]{/color}"

    scene bg black with dissolve
    
    if weather == 1:    # Sunny
        "You get out of your car to the feeling of the warmth sun on your face. The sky is blue, the birds are singing, and there's green all around you."
        "You open the door to the office and as you step down the staircase, your surroundings become increasingly cold, fluorescent, and {i}beige{/i}."

    elif weather == 2:  # Rainy
        "You enjoy the feeling of the rain and the brisk air as you make your way from the car to the office. For a moment, you feel relaxed."
        "You open the door to the office and as you step down the staircase, your surroundings become increasingly cold, fluorescent, and {i}beige{/i}."

    else:               # Gray sky
        "You get out of your car. You make your way from the cold, dreary outside into the cold dreary inside."
        
    $ event = renpy.random.randint( 1, 3 )
    
    if event == 1:
        "You pass by a coworker on your way to your desk. You each exchange automated \"Hi, how are you?\" \"I'm good, and you?\" greetings as you pass."
    
    else:
        "You make your way to your desk."
    
    return


# --------------------------------------------------------- START OF DAY
# --------------------------------------------------------- Reset daily variables
label reset_daily_variables:
    $ timeMinutes = 0
    $ timeHours = 5
    $ totalBreaksToday = 0
    $ totalNoticableBreaktimeToday = 0
    $ totalDiscreetBreaktimeToday = 0
    $ lunchesTakenToday = 0
    $ workToBeDone = workToBeDone + renpy.random.randint( 4, 12 )
    $ timeRemainingMinutes = 8 * 60

    return


# ------------------------------------------------------ DAY CYCLE START
label cycle_start:
    if goodEmployeeRating <= 10:
        jump fired
        return

    scene bg computer_coding with dissolve

    call restrict_vars from _call_restrict_vars_1
    call menu_level_1 from _call_menu_level_1

    jump cycle_start


# ------------------------------------------------------ DAY CYCLE START
label menu_level_1:
    call stats() from _call_stats
    
    if workToBeDone <= 0 and timeHours < 15:
        $ workToBeDone = workToBeDone + renpy.random.randint( 4, 8 )
        $ goodEmployeeRating += 10
    
        $ energyChange = 10
        $ energy -= energyChange    
        $ depressionChange = 5
        $ depression += depressionChange
        $ alienationChange = 10
        $ alienation += alienationChange
        
        boss "Oh, I see that you've finished your work for the day."
        boss "Well, there's always more work to be done! Here are some more tasks for you..."
        "You receive [workToBeDone] more tickets for the day."
        
        play sound "sounds/sad.ogg"
        "The sudden dump of extra work saps your energy. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy -[energyChange]{/color}      {color=#FF0000} Alienation +[alienationChange]{/color}"

        return

    if energy < 30:  
        $ depressionChange = 2
        $ depression += depressionChange
        
        play sound "sounds/sad.ogg"
        "Your energy is getting low.\n {color=#FF0000} Depression +[depressionChange]{/color}"
        
        call stats() from _call_stats_1
    
    if timeRemainingMinutes > 8*60:     # Working past the clock, but you're salaried
        $ energyChange = 2
        $ energy -= energyChange    
        $ depressionChange = 2
        $ depression += depressionChange
        
        play sound "sounds/sad.ogg"
        "As you continue working past 8 hours, you become less and less productive and more and more tired. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy -[energyChange]{/color}"

        call stats() from _call_stats_2
    
    if timeHours > 17:                  # Working after 5 pm
        $ energyChange = 2
        $ energy -= energyChange    
        $ depressionChange = 2
        $ depression += depressionChange
        
        play sound "sounds/sad.ogg"
        "As your day is being eaten up by being at the office, you feel more depressed over having less free time for the day. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy -[energyChange]{/color}"

        call stats() from _call_stats_3
    
    call stats() from _call_stats_4

    menu:
        "Do some work":
            call menu_level_2a_DoWork from _call_menu_level_2a_DoWork

        "Take a break":
            call menu_level_2b_TakeBreak from _call_menu_level_2b_TakeBreak

        "Slack off at your desk":
            call menu_level_2c_SlackOff from _call_menu_level_2c_SlackOff

        "Go home":
            call menu_level_2d_GoHome from _call_menu_level_2d_GoHome

    return

# ------------------------------------------------------ DAY CYCLE START
# ------------------------------------------------------ Do work...
label menu_level_2a_DoWork:
    if workToBeDone == 0:
        "There's no more work to be done!"
    else:
    
        call stats() from _call_stats_5
        
        $ event = renpy.random.randint( 1, 10 )
        
        $ workToBeDone -= 1
        
        $ energyChange = renpy.random.randint( 2, 10 )
        $ energy -= energyChange
        
        play sound "sounds/typing.ogg"
        
        if event == 1:
            $ minutes = renpy.random.randint( 15, 30 )
            "You add a small new feature. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 2:
            $ minutes = renpy.random.randint( 60, 90 )
            "The ticket for this task lacked proper documentation, so you had to spend extra time to figure out what was even required. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 3:
            $ minutes = renpy.random.randint( 45, 90 )
            "While working on this task, you got a message about an existing bug, and had to help diagnose it before continuing on. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 4:
            $ minutes = renpy.random.randint( 15, 30 )
            "You do a refactoring task. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 5:
            $ minutes = renpy.random.randint( 15, 30 )
            "You write some unit tests. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 5:
            $ minutes = renpy.random.randint( 60, 90 )
            "You try to solve a tricky bug, but it takes a long time to reproduce. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 6:
            $ minutes = renpy.random.randint( 5, 15 )
            "You clean up the user interface a little bit. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 7:
            $ minutes = renpy.random.randint( 5, 15 )
            "You write some query scripts. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 8:
            $ minutes = renpy.random.randint( 5, 30 )
            "You finish testing that thing you forgot to test yesterday. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 9:
            $ minutes = renpy.random.randint( 10, 15 )
            "You pull the latest code from the master branch and have to spend time manually merging things. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
            
        elif event == 10:
            $ minutes = renpy.random.randint( 5, 15 )
            "You write some documentation for the task you're working on. \n{color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"


        $ timeMinutes += minutes
        $ timeRemainingMinutes -= minutes


    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Take break...
label menu_level_2b_TakeBreak:
    call stats() from _call_stats_6
    # goodEmployeeRating
    # energybg breakroom
    menu:
        "Coffee break":
            call menu_level_2ba_CoffeeBreak from _call_menu_level_2ba_CoffeeBreak

        "Smoke break":
            call menu_level_2bb_SmokeBreak from _call_menu_level_2bb_SmokeBreak

        "Bathroom break":
            call menu_level_2bc_BathroomBreak from _call_menu_level_2bc_BathroomBreak

        "Lunch break":
            call menu_level_2bd_LunchBreak from _call_menu_level_2bd_LunchBreak

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Take break...
# ------------------------------------------------------------   Coffee break
label menu_level_2ba_CoffeeBreak:
    scene bg breakroom with dissolve

    $ event = renpy.random.randint( 1, 10 )

    $ minutes = 5
    
    if event == 1:
        $ minutes += 1
        
        "As you walk to the lounge, somebody is coming down the same hall in the opposite direction."
        "You each waste some time stepping from one side to the other to try to make space."

    elif event == 2:
        $ minutes += 2
        
        "In the lounge, there is somebody ahead of you getting coffee. It takes a little longer."

    elif event == 3:
        $ minutes += 10
        
        "The coffee pot was left empty. You start a new pot of coffee and it takes a while."

    $ totalNoticableBreaktimeToday += 5
    $ totalBreaksToday += 1
    $ energy += 5
    $ depression -= 1

    $ timeRemainingMinutes -= minutes
    $ timeMinutes += minutes

    if totalBreaksToday > 4:
        $ stealth -= 1

    play sound "sounds/coffee.ogg"
    "The coffee warms your body and wakes you up a bit.\n{color=#00FF00} Depression -2{/color}      {color=#00FF00} Energy +5{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
    
    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Take break...
# ------------------------------------------------------------   Smoke break
label menu_level_2bb_SmokeBreak:
    scene bg outside with dissolve

    $ totalNoticableBreaktimeToday += 10
    $ totalBreaksToday += 1
    $ energy += 5

    $ minutes = 10
    $ timeMinutes += minutes
    $ timeRemainingMinutes -= minutes
    
    play sound "sounds/smoking.ogg"
    "You know it isn't good for you, but really, what's the point of living {i}longer{/i}, anyway? \n{color=#00FF00} Energy +5{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Take break...
# ------------------------------------------------------------   Bathroom break
label menu_level_2bc_BathroomBreak:
    scene bg bathroom with dissolve

    $ event = renpy.random.randint( 1, 6 )

    $ minutes = 0

    if event == 1:
        $ minutes = renpy.random.randint( 1, 2 )
        
        $ energyChange = 2
        $ energy += energyChange
        
        play sound "sounds/flush.ogg"
        "You have the bathroom to yourself and you pee in peace. \n{color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 2:
        $ minutes = renpy.random.randint( 5, 10 )
        
        $ energyChange = 2
        $ energy += energyChange
        $ depressionChange = 1
        $ depression += depressionChange
        
        play sound "sounds/flush.ogg"
        "Somebody was already in the bathroom when you got in. You {i}try{/i} to pee, but you have a shy bladder. You sit there in silence until the other person leaves, and you finally pee. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 3:
        $ minutes = renpy.random.randint( 5, 15 )
        
        $ energyChange = 1
        $ energy += energyChange
        $ depressionChange = 2
        $ depression += depressionChange
        
        play sound "sounds/flush.ogg"
        "You make yourself comfy in the stall, but then a coworker comes in and {i}sits in the stall neighboring yours{/i}. Your frustration and anxiety build, as you wait for the other person to leave so you can pee. You complain about the other person on social media. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
        
    elif event == 4:
        $ minutes = 1
        
        $ energyChange = 2
        $ energy += energyChange
        
        play sound "sounds/blownose.ogg"
        "You stop by the bathroom to blow your nose. \n{color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 5:
        $ minutes = renpy.random.randint( 2, 4 )
        
        $ energyChange = 5
        $ energy += energyChange
        $ depressionChange = 5
        $ depression -= depressionChange
        
        "You fix your hair in the bathroom, and maybe take a selfie or two. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 6:
        $ minutes = renpy.random.randint( 1, 2 )
        
        $ energyChange = 5
        $ energy += energyChange
        $ depressionChange = 5
        $ depression -= depressionChange

        play sound "sounds/flush.ogg"
        "You use the toilet, but also spend some extra time on your phone. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
        
        
    $ totalDiscreetBreaktimeToday += minutes
    $ totalBreaksToday += 1

    $ timeRemainingMinutes -= minutes
    $ timeMinutes += minutes

    if totalBreaksToday > 4:
        $ stealth -= 1

    return

# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Take break...
# ------------------------------------------------------------   Lunch break
label menu_level_2bd_LunchBreak:
    if lunchesTakenToday >= 1:
        play sound "sounds/sad.ogg"
        "{color=#FF0000}You already took lunch today! \nAre you sure you want to go again?{/color}"
        menu:
            "Yes":
                pass

            "No":
                jump cycle_start
    else:
        pass

    scene bg lunch with dissolve

    $ totalNoticableBreaktimeToday += 60
    $ lunchesTakenToday += 1
    $ timeHours += 1
    $ minutes = 60
    
    $ energyChange = 20
    $ energy += energyChange
    $ depressionChange = 10
    $ depression -= depressionChange

    play sound "sounds/happy.ogg"
    "You take an hour lunch break. \n{color=#FF0000} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"
    
    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Slack off...
label menu_level_2c_SlackOff:
    call stats() from _call_stats_7

    menu:
        "Pretend to work":
            call menu_level_2ca_Pretend from _call_menu_level_2ca_Pretend

        "Surf the web":
            call menu_level_2cb_Articles from _call_menu_level_2cb_Articles

        "Sketch":
            call menu_level_2cc_Sketch from _call_menu_level_2cc_Sketch
            
        "Watch videos":
            call menu_level_2cd_Videos from _call_menu_level_2cd_Videos

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Slack off...
# ------------------------------------------------------------   Pretend to work
label menu_level_2ca_Pretend:
    $ event = renpy.random.randint( 1, 4 )

    $ minutes = 0

    if event == 1:
        $ stealth += 4
        $ minutes = 15
        
        $ depressionChange = 3
        $ depression += depressionChange
        $ alienationChange = 3
        $ alienation += alienationChange
        
        play sound "sounds/typing.ogg"
        "You open a small window and type gibberish for a while to make it sound like you're working. You literally can't think of anything else to do to fill the time. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Alienation +[alienationChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 2:
        $ stealth += 2
        $ minutes = renpy.random.randint( 1, 5 )
        
        $ depressionChange = 2
        $ depression += depressionChange
        $ alienationChange = 2
        $ alienation += alienationChange
        
        play sound "sounds/typing.ogg"
        "You aimlessly click through code files to make it seem like you're working, but you don't feel like {i}actually{/i} coding anything. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Alienation +[alienationChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 3:
        $ stealth += 2
        $ minutes = renpy.random.randint( 5, 20 )
        
        $ depressionChange = 1
        $ depression += depressionChange
        $ alienationChange = 1
        $ alienation += alienationChange
        
        play sound "sounds/typing.ogg"
        "You write a draft blog post in the code editor. Maybe you'll post it online later? Eh. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Alienation +[alienationChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 4:
        
        scene bg computer_chatting with dissolve
        
        $ stealth += 2
        $ minutes = renpy.random.randint( 5, 20 )
        
        $ depressionChange = 3
        $ depression -= depressionChange
        $ alienationChange = 3
        $ alienation -= alienationChange
        
        play sound "sounds/typing.ogg"
        "You chat with a coworker. The text size is small, so any passersby probably think you're talking about important stuff. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Alienation -[alienationChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    $ timeMinutes += minutes
    $ timeRemainingMinutes -= minutes
    $ totalDiscreetBreaktimeToday += minutes

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Slack off...
# ------------------------------------------------------------   Read articles
label menu_level_2cb_Articles:
    $ event = renpy.random.randint( 1, 4 )

    $ minutes = 0

    if event == 1:
        scene bg computer_web with dissolve

        $ minutes = renpy.random.randint( 3, 5 )
        
        $ depressionChange = 3
        $ depression -= depressionChange
        $ alienationChange = 3
        $ alienation -= alienationChange
        $ energyChange = 3
        $ energy += energyChange
        
        "You read a \"Top 10\" list article on some throwaway website. At least it's entertaining. \n{color=#00FF00} Alienation -[alienationChange]{/color}      {color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"


    elif event == 2:
        scene bg computer_web with dissolve

        $ minutes = renpy.random.randint( 3, 8 )
        
        $ depressionChange = 5
        $ depression += depressionChange
        $ alienationChange = 3
        $ alienation -= alienationChange
        $ energyChange = 5
        $ energy -= energyChange
        
        play sound "sounds/mouse.ogg"
        "You read a news article, and now you feel worse about the world. \n{color=#FF0000} Alienation -[alienationChange]{/color}      {color=#00FF00} Depression +[depressionChange]{/color}      {color=#FF0000} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 3:
        scene bg computer_web with dissolve

        $ minutes = renpy.random.randint( 5, 10 )
        
        $ depressionChange = 1
        $ depression += depressionChange
        $ alienationChange = 3
        $ alienation -= alienationChange
        $ energyChange = 5
        $ energy += energyChange
        
        play sound "sounds/mouse.ogg"
        "You check social media briefly. \n{color=#00FF00} Alienation -[alienationChange]{/color}      {color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy -[energyChange]{/color}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 4:
        scene bg computer_web with dissolve

        $ minutes = renpy.random.randint( 30, 45 )
        $ stealth -= 2
        
        $ depressionChange = 2
        $ depression -= depressionChange
        $ alienationChange = 1
        $ alienation += alienationChange
        
        play sound "sounds/mouse.ogg"
        "You check social media, but accidentally get engrossed in a flame war. \n{color=#FF0000} Alienation +[alienationChange]{/color}      {color=#FF0000} Depression +[depressionChange]{/color}      {color=#00FF00} Minutes passed: [minutes] {/color}"


    $ totalNoticableBreaktimeToday += minutes
    $ totalBreaksToday += 1

    $ timeRemainingMinutes -= minutes
    $ timeMinutes += minutes

    if totalBreaksToday > 4:
        $ stealth -= 1

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Slack off...
# ------------------------------------------------------------   Sketch
label menu_level_2cc_Sketch:
    $ event = renpy.random.randint( 1, 3 )

    $ minutes = 0

    if event == 1:
        $ minutes = renpy.random.randint( 1, 5 )
        $ depression -= 3
        $ energy += 3
        
        $ depressionChange = 3
        $ depression -= depressionChange
        $ energyChange = 5
        $ energy += energyChange
        
        play sound "sounds/sketching.ogg"
        "You sketch a little picture. Hopefully everybody else thinks you're diagramming something software-related. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#00FF00}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 2:
        $ minutes = renpy.random.randint( 5, 10 )
        $ stealth -= 2
        
        $ depressionChange = 7
        $ depression -= depressionChange
        $ energyChange = 7
        $ energy += energyChange
        
        play sound "sounds/sketching.ogg"
        "You draw a little comic. It's cute, but doing that wasn't very discreet since it took a while. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#00FF00}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 3:
        $ minutes = renpy.random.randint( 5, 10 )
        $ stealth -= 1
        
        $ depressionChange = 2
        $ depression -= depressionChange
        $ energyChange = 2
        $ energy += energyChange
        
        play sound "sounds/sketching.ogg"
        "You make a little doodle on the computer, keeping the paint window in a tiny corner. Hopefully nobody noticed. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}       {color=#FFFF00} Minutes passed: [minutes] {/color}"

    $ totalNoticableBreaktimeToday += minutes
    $ totalBreaksToday += 1

    $ timeRemainingMinutes -= minutes
    $ timeMinutes += minutes

    if totalBreaksToday > 4:
        $ stealth -= 1

    return
    
    
# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Slack off...
# ------------------------------------------------------------   Videos
label menu_level_2cd_Videos:
    $ event = renpy.random.randint( 1, 3 )

    $ minutes = 0
    
    scene bg computer_videos with dissolve

    if event == 1:
        $ minutes = renpy.random.randint( 1, 5 )
        
        $ depressionChange = 5
        $ depression -= depressionChange
        $ energyChange = 5
        $ energy += energyChange
        
        "You watch a video of kittens and puppies. At least there's one thing right with the world. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#00FF00}       {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 2:
        $ minutes = renpy.random.randint( 5, 10 )
        $ stealth -= 2
        
        $ depressionChange = 7
        $ depression -= depressionChange
        $ energyChange = 7
        $ energy += energyChange
        
        "You watch a video about an upcoming video game. You feel a little excited for it. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#00FF00}      {color=#FFFF00} Minutes passed: [minutes] {/color}"

    elif event == 3:
        $ minutes = renpy.random.randint( 10, 12 )
        $ stealth -= 1
        
        $ depressionChange = 4
        $ depression -= depressionChange
        $ energyChange = 4
        $ energy += energyChange
        
        "You watch an episode of a Let's Play from your favorite duo. \n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}      {color=#00FF00}       {color=#FFFF00} Minutes passed: [minutes] {/color}"

    $ totalNoticableBreaktimeToday += minutes
    $ totalBreaksToday += 1

    $ timeRemainingMinutes -= minutes
    $ timeMinutes += minutes

    if totalBreaksToday > 4:
        $ stealth -= 1

    return


# ------------------------------------------------------------ DAY CYCLE
# ------------------------------------------------------------ Go home...
label menu_level_2d_GoHome:
    call stats() from _call_stats_8
    # Have you met the work hours?
    # Affect "goodEmployeeRating" if you leave too early.

    $ leave = True
    $ warning = False

    if timeRemainingMinutes > 0:
        play sound "sounds/sad.ogg"
        "{color=#FF0000}You haven't been at the office for a total of 8 hours today. Leaving now is considered leaving early.{/color}"
        $ warning = True

    if timeHours < 16 and leave == True:
        play sound "sounds/sad.ogg"
        "{color=#FF0000}It's techically still within core business hours. Leaving now is considered too early.{/color}"
        $ warning = True

    if warning == True:
        menu:
            "Leave anyway":
                $ leave = True
                
            "Nevermind":
                $ leave = False

    if leave == True:
        # Leaving early before 8 hours
        if timeRemainingMinutes > 0:
            $ goodEmployeeRating -= 10

        # Leaving early before 4:00 pm
        if timeHours < 16:
            $ goodEmployeeRating -= 10

        jump end_of_day

    return


# ------------------------------------------------------- END OF THE DAY
label end_of_day:
    call end_of_day_stats() from _call_end_of_day_stats

    menu:
        "End day":
            pass
    
    stop music fadeout 1.0
    scene bg black with dissolve
    
    # Dinner time
    if depression > 50:
        $ event = renpy.random.randint( 1, 3 )
        
        if event == 1:
            $ depressionChange = 1
            $ depression -= depressionChange
            $ energyChange = 3
            $ energy += energyChange
            
            play sound "sounds/sad.ogg"
            "You're too depressed to make dinner tonight. You grab some fast food tacos instead.\n{color=#00FF00} Depression -[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}"
            
        elif event == 2:
            $ depressionChange = 2
            $ depression += depressionChange
            $ energyChange = 1
            $ energy += energyChange
            
            play sound "sounds/sad.ogg"
            "You're too depressed to make dinner tonight. You eat some tortilla chips.\n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}"
            
        elif event == 4:
            $ depressionChange = 4
            $ depression += depressionChange
            $ energyChange = 4
            $ energy += energyChange
            
            play sound "sounds/sad.ogg"
            "You're too depressed to make dinner tonight. You skip dinner.\n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#00FF00} Energy +[energyChange]{/color}"
    
    # Sleepin'
    if ( depression > 75 ):
        $ depressionChange = 2
        $ depression += depressionChange
        $ energyChange = 40
        $ energy += energyChange
        
        play sound "sounds/sad.ogg"
        "You have trouble falling asleep tonight and fail to get a full night of sleep. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy +[energyChange]{/color}"
        
    elif ( depression > 50 ):
        $ event = renpy.random.randint( 1, 5 )
        
        if event == 1:
            $ depressionChange = 1
            $ depression += depressionChange
            $ energyChange = 50
            $ energy += energyChange
            
            play sound "sounds/sad.ogg"
            "You have trouble staying asleep, so you don't get enough rest. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy +[energyChange]{/color}"
            
        elif event == 2:
            $ depressionChange = 2
            $ depression += depressionChange
            $ energyChange = 40
            $ energy += energyChange
            
            play sound "sounds/sad.ogg"
            "You stay up too late playing video games because you don't want it to be tomorrow already. You don't get enough sleep. \n{color=#FF0000} Depression +[depressionChange]{/color}      {color=#FF0000} Energy +[energyChange]{/color}"
        
        elif event == 3:
            $ energyChange = 70
            $ energy += energyChange
            
            play sound "sounds/happy.ogg"
            "You're so done with the day. You fall asleep quickly. \n{color=#00FF00} Energy +[energyChange]{/color}"
            
    else:
        $ energyChange = 90
        $ energy += energyChange
        
        play sound "sounds/happy.ogg"
        "You feel like you've had a complete day. You fall asleep quickly. \n{color=#00FF00} Energy +[energyChange]{/color}"
            
    $ day += 1
    if day > 7:
        $ day = 1
    
    jump day_start
    return
    
    

# ---------------------------------------------------------------- FIRED
label fired:
    boss "I've had enough. You're fired."

    play sound "sounds/sad.ogg"
    "You've lost your job. Now how will you pay off your student loans??"
    $ gameOver = True

    return


# ----------------------------------------------------------- VIEW STATS
