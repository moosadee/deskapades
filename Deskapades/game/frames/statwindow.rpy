label stats():
    call format_time from _call_format_time_1
    call format_timeLeft from _call_format_timeLeft_1
    call restrict_vars from _call_restrict_vars_2

    $ stats_frame()
    $ stats_frame2()
    return

init python:
    import time

    def stats_frame():
        headerFont = 17
        defaultFont = 15
        subFont = 12

        ui.frame( xfill=False, yminimum=None, xalign=.02, yalign=.02, left_padding = 20, right_padding = 20, top_padding = 20, bottom_padding = 20 )

        ui.vbox( top_padding = 20 )

        ui.text( "[companyName]", size=headerFont )
        
        ui.text( "", size=defaultFont )
        ui.text( "Time: [timeText]", size=defaultFont )
        ui.text( "(core hours, 9:00 am - 4:00 pm)", size=subFont )
        ui.text( "", size=defaultFont )
        ui.text( "Work time remaining: [timeRemainingText]", size=defaultFont )
        ui.text( "(8 hrs/day)", size=subFont )
        ui.text( "", size=defaultFont )
        ui.text( "Tasks left: [workToBeDone]", size=defaultFont )
        ui.text( "", size=defaultFont )
        
        if goodEmployeeRating >= 90:
            ui.text( "Manager opinion: Star coder", size=defaultFont )
        
        elif goodEmployeeRating >= 70:
            ui.text( "Manager opinion: Reliable", size=defaultFont )
        
        elif goodEmployeeRating >= 50:
            ui.text( "Manager opinion: \"Not my first choice\"", size=defaultFont )
        
        elif goodEmployeeRating >= 30:
            ui.text( "Manager opinion: Too much trouble to fire you", size=defaultFont )
        
        else:
            ui.text( "Manager opinion: On thin ice", size=defaultFont )
        
        
        ui.text( "", size=defaultFont )
        
        if stealth >= 90:
            ui.text( "Slacking stealth: Inconspicuous", size=defaultFont )
            
        elif stealth >= 75:
            ui.text( "Slacking stealth: Gettin' stuff done", size=defaultFont )
            
        elif stealth >= 50:
            ui.text( "Slacking stealth: Working hard or hardly working?", size=defaultFont )
            
        elif stealth >= 30:
            ui.text( "Slacking stealth: Too obvious", size=defaultFont )
            
        else:
            ui.text( "Slacking stealth: Why haven't you been fired yet?", size=defaultFont )
        
        ui.close()

    def stats_frame2():
        headerFont = 25
        defaultFont = 20
        subFont = 12

        ui.frame( xfill=False, yminimum=None, xalign=.98, yalign=.02, left_padding = 20, right_padding = 20, top_padding = 20, bottom_padding = 20 )

        ui.vbox( top_padding = 20 )

        ui.text( "Energy: [energy]%", size=defaultFont )
        ui.text( "Depression: [depression]%", size=defaultFont )
        ui.text( "Alienation: [alienation]%", size=defaultFont )

        ui.close()
