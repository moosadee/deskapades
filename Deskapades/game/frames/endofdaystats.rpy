label end_of_day_stats():
    call restrict_vars from _call_restrict_vars
    call format_time from _call_format_time
    call format_timeLeft from _call_format_timeLeft

    $ end_of_day_stats_frame()
    return

init python:
    import time

    def end_of_day_stats_frame():
        
        warningSize = 15

        ui.frame( xfill=False, yminimum=None, xalign=.5, yalign=.1, left_padding = 20, right_padding = 20, top_padding = 20, bottom_padding = 20 )

        ui.vbox( top_padding = 20 )

        ui.text( "Another day at [companyName] complete!", size=25 )
        ui.text( "", size=20 )
        ui.text( "Time you left the office: [timeText]", size=20 )
        if workToBeDone > 1:
            ui.text( "Tasks left: {color=#FF0000}[workToBeDone]{/color}", size=20 )
        else:
            ui.text( "Tasks left: {color=#00FF00}[workToBeDone]{/color}", size=20 )
        ui.text( "Employee rating: [goodEmployeeRating]%", size=20 )
        ui.text( "", size=20 )
        ui.text( "Total noticable breaktime: [totalNoticableBreaktimeToday] minutes", size=20 )
        ui.text( "Total discreet breaktime: [totalDiscreetBreaktimeToday] minutes", size=20 )
#        ui.text( "Total breaks: [totalBreaksToday] minutes", size=20 )
        ui.text( "", size=20 )
        ui.text( "Energy: [energy]%     Depression: [depression]%       Alienation: [alienation]%", size=20 )
        ui.text( "", size=20 )
        ui.text( "PTO: [pto]", size=20 )

        ui.text( "", size=20 )
        
        if timeHours < 16:
            ui.text( "Your boss is unhappy that you left during core hours.", size=warningSize )
            
        if timeRemainingMinutes > 0:
            ui.text( "Your boss is unhappy that you haven't been at the office for 8 hours.", size=warningSize )
        
        if totalBreaksToday > 4:
            ui.text( "Your boss is unhappy at the amount of breaks you took.", size=warningSize )
        
        if workToBeDone > 1:
            ui.text( "Your boss is unhappy about the leftover work.", size=warningSize )
        
        if lunchesTakenToday > 1:
            ui.text( "Your boss is unhappy that you took multiple lunch breaks.", size=warningSize )
        
        if pto < 0:
            ui.text( "Your boss is unhappy that you've taken too much Paid Time Off.", size=warningSize )
        
        ui.close()
