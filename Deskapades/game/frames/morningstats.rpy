label morning_time():
    call format_time from _call_format_time_2
    call format_timeLeft from _call_format_timeLeft_2
    call restrict_vars from _call_restrict_vars_3

    $ time_frame()
    return

init python:
    import time

    def time_frame():
        defaultFont = 40

        ui.frame( xfill=False, yminimum=None, xalign=.5, yalign=.5, left_padding = 20, right_padding = 20, top_padding = 20, bottom_padding = 20 )

        ui.vbox( top_padding = 20 )

        ui.text( "Time: [timeText]", size=defaultFont )
        
        ui.close()
